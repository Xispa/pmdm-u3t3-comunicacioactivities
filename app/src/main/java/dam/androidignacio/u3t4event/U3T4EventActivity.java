package dam.androidignacio.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class U3T4EventActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3t4event);

        setUI();
    }

    private void setUI() {
        bundle = new Bundle();
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);
        // clean text view
        tvCurrentData.setText("");
    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);

        // TODO Extra
        if (etEventName.getText().toString().isEmpty()) {
            // if event name is empty set default name
            bundle.putString("EventName", getResources().getString(R.string.tvNewEvent));
        } else {
            // set info data to bundle
            bundle.putString("EventName", etEventName.getText().toString());
        }

        // TODO Ex1.1
        bundle.putString("EventData", tvCurrentData.getText().toString());
        // add bundle to intent
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if result comes from EventDataActivity and finished OK
        if (requestCode == REQUEST && resultCode == RESULT_OK) {
            tvCurrentData.setText(data.getStringExtra("EventData"));
            etEventName.setText(data.getStringExtra("etEventName"));
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("EventData", tvCurrentData.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        tvCurrentData.setText(savedInstanceState.getString("EventData"));
    }
}