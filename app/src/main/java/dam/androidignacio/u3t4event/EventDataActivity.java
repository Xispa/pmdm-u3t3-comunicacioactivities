package dam.androidignacio.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity
                            implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String priority = "Normal";
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private EditText etPlace;
    private String[] months;
    private String min;
    private String place;

    // TODO Ex1.1
    private Bundle inputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        // get data from Intent that started this activity
        inputData = getIntent().getExtras();

        setUI();

        // set eventName from inputData
        tvEventName.setText(inputData.getString("EventName"));

        // TODO Ex1.2
         months = getResources().getStringArray(R.array.months);

    }

    private void setUI() {
        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        // TODO Ex1.3
        etPlace = findViewById(R.id.etPlace);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        setData();

    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (v.getId()) {
            case R.id.btAccept:
                // si els minuts del TimePicker son menors de 10 escric un 0 davant
                if (tpTime.getMinute() < 10) {
                    min = "0" + tpTime.getMinute();
                } else {
                    min = String.valueOf(tpTime.getMinute());
                }
                // si el lloc està buit escric un lloc per defecte
                if (etPlace.getText().toString().isEmpty()) {
                    place = getString(R.string.defPlace);
                } else {
                    place = etPlace.getText().toString();
                }
                String placeData = getString(R.string.place) + " " + place;
                String priorityData = getString(R.string.priority) + " " + priority;
                String dateData = getString(R.string.date) + " " + dpDate.getDayOfMonth() + " " +
                        months[dpDate.getMonth()] + " " + dpDate.getYear();
                String timeData = getString(R.string.hour)  + " " + tpTime.getHour() + ":" + min;
                // passe totes les dades al bundle
                eventData.putString("EventData", placeData + "\n" +
                                                 priorityData + "\n" +
                                                 dateData + "\n" +
                                                 timeData);
                break;
            case R.id.btCancel:
                // TODO Ex1.1
                eventData.putString("EventData", inputData.getString("EventData"));
                break;
        }
        eventData.putString("etEventName", tvEventName.getText().toString());
        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rbLow:
                priority = getString(R.string.rbLow);
                break;
            case R.id.rbNormal:
                priority = getString(R.string.rbNormal);
                break;
            case R.id.rbHigh:
                priority = getString(R.string.rbHigh);
                break;
        }
    }

    private void setData() {
        if (!inputData.getString("EventData").isEmpty()) {
            String[] dades = inputData.getString("EventData").split("\n");

            String[] lloc = dades[0].split(" ");
            etPlace.setText(lloc[1]);

            setPriorityCheck(dades);

            String[] data = dades[2].split(" ");
            dpDate.updateDate(Integer.parseInt(data[3]), getMonth(data[2]), Integer.parseInt(data[1]));

            String[] temps = dades[3].split(" ");
            String[] hora = temps[1].split(":");
            tpTime.setHour(Integer.parseInt(hora[0]));
            tpTime.setMinute(Integer.parseInt(hora[1]));
        }
    }

    private void setPriorityCheck(String[] dades) {
        String[] priority = dades[1].split(" ");
        switch (priority[1]) {
            case "Low":
            case "Baixa":
            case "Baja":
                rgPriority.check(R.id.rbLow);
                break;
            case "Normal":
                rgPriority.check(R.id.rbNormal);
                break;
            case "High":
            case "Alta":
                rgPriority.check(R.id.rbHigh);
                break;
        }

    }

    private int getMonth(String month) {
        switch (month) {
            case "January":
            case "Gener":
            case "Enero":
                return 0;
            case "February":
            case "Febrer":
            case "Febrero":
                return 1;
            case "March":
            case "Març":
            case "Marzo":
                return 2;
            case "April":
            case "Abril":
                return 3;
            case "May":
            case "Maig":
            case "Mayo":
                return 4;
            case "June":
            case "Juny":
            case "Junio":
                return 5;
            case "July":
            case "Juliol":
            case "Julio":
                return 6;
            case "August":
            case "Agost":
            case "Agosto":
                return 7;
            case "September":
            case "Setembre":
            case "Septiembre":
                return 8;
            case "October":
            case "Octubre":
                return 9;
            case "November":
            case "Novembre":
            case "Noviembre":
                return 10;
            default:
                return 11;
        }
    }
}